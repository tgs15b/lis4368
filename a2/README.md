> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced WebApplications Development

## Tucker Southerland

### Assignment #2 Requirements:

1. http://localhost:9999/hello (displays directory, needs index.html)
2. http://localhost:9999/hello/HelloHome.html
(Rename "HelloHome.html" to "index.html")
3. http://localhost:9999/hello/sayhello (invokes HelloServlet)
Note: /sayhello maps to HelloServlet.class (changed web.xml file)
4. http://localhost:9999/hello/querybook.html
5. http://localhost:9999/hello/sayhi (invokes AnotherHelloServlet)


#### README.md file should include the following items:

* Screenshot of http://localhost:9999/hello/querybook.html



#### Assignment Screenshots:


*Screenshot of querybook:*

![Querybook Completion Screenshot](img/using_servlets.png)


