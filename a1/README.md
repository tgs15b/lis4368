> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced WebApplications Development

## Tucker Southerland

### Assignment #1 Requirements:

Three Parts

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running http://localhost9999
* git commands with short descriptions 
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates empty git rep
2. git status - shows the status of staging
3. git add - add files to the staging area
4. git commit - commit files that are in the staging area to the repo
5. git push - push the files to the repo
6. git pull - grab files from the reposititory 
7. git remote - add an origin link to the repo on local machine 

#### Assignment Screenshots:


*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk.png)


*Screenshot of running http://localhost:9999

![Tom Cat Running](img/tomcat.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://tgs15b@bitbucket.org/tgs15b/bitbucketstationlocations.git/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://tgs15b@bitbucket.org/tgs15b/myteamquotes.git/ "My Team Quotes Tutorial")
