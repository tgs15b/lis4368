-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema tgs15b
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `tgs15b` ;

-- -----------------------------------------------------
-- Schema tgs15b
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tgs15b` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `tgs15b` ;

-- -----------------------------------------------------
-- Table `tgs15b`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tgs15b`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tgs15b`.`customer` (
  `cus_id` SMALLINT UNSIGNED NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT NOT NULL,
  `cus_phone` BIGINT NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `tgs15b`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tgs15b`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tgs15b`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_phone` BIGINT NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_vtd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `tgs15b`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tgs15b`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tgs15b`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `petstore_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`petstore_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`petstore_id`)
    REFERENCES `tgs15b`.`petstore` (`pst_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `tgs15b`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `tgs15b`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `tgs15b`;
INSERT INTO `tgs15b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (NULL, 'Tucker', 'Southerland', '2408 Delgado Dr', 'Jacksonville', 'FL', 32246, 9043144625, 'tucker@gmail.com', 23, 25, NULL);
INSERT INTO `tgs15b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (NULL, 'Mark', 'Jowett', '56 Lapona Dr', 'Tallahassee', 'FL', 32304, 8509034340, 'Mark@gmail.com', 32, 60, NULL);
INSERT INTO `tgs15b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (NULL, 'Johnny', 'Heiss', '40 NW Ave', 'Tampa', 'FL', 32278, 8500930933, 'Heiss@gmail.com', 23, 45, NULL);
INSERT INTO `tgs15b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (NULL, 'Austin ', 'Marvin', '67 NS Ave', 'Jacksonville', 'FL', 32222, 9048900989, 'djhotmarv@gmail.com', 23, 45, NULL);
INSERT INTO `tgs15b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (NULL, 'Tommy', 'Frank', '23 NE Ave', 'Jacksonville', 'FL', 32233, 9043144600, 'frank@gmail.com', 12, 67, NULL);
INSERT INTO `tgs15b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (NULL, 'Luke ', 'Skywalker', '23 Deathstar Ln', 'Galaxy', 'FL', 32233, 9045678000, 'skywalker@gmail.com', 13, 100, NULL);
INSERT INTO `tgs15b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (NULL, 'John', 'Odea', '720 Hayden Rd', 'Tallahassee', 'FL', 32304, 9049509090, 'odea@gmail.com', 10, 200, NULL);
INSERT INTO `tgs15b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (NULL, 'Dean', 'Atkinson', '60 SE Ave', 'Tallahassee', 'FL', 32304, 9046788900, 'atkinson@gmail.com', 19, 299, NULL);
INSERT INTO `tgs15b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (NULL, 'Taylor ', 'Ludwig', '30 SW Ave ', 'Tampa', 'FL', 32267, 8090900000, 'ludwig@gmail.com', 12, 122, NULL);
INSERT INTO `tgs15b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (NULL, 'Alexander', 'Duke', '40 Bayshore Dr', 'Tampa', 'FL', 32233, 9043434555, 'guysbeindudes@aol.com', 19, 233, NULL);
INSERT INTO `tgs15b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (NULL, 'Shanta ', 'Feliz', '70 Bay Ave NW', 'Tallhassee', 'FL', 32304, 9034930490, 'yoyoyo@gmail.com', 12, 233, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `tgs15b`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `tgs15b`;
INSERT INTO `tgs15b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (NULL, 'Pet Supermarket', '300 Northwest Crt', 'Tallahassee', 'GA', 9040909000, 'supermarket@gmail.com', 'supermarket.com', 500000, NULL);
INSERT INTO `tgs15b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (NULL, 'Pets and things', '4000 West Blvd', 'Tallahassee', 'GA', 9047868900, 'petandthing@gmail.com', 'petandthing.com', 700000, NULL);
INSERT INTO `tgs15b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (NULL, 'Claws and Paws', '200 NW Crt', 'Tallahassee', 'FL', 5060909500, 'claws@gmail.com', 'clawspaws.com', 500000, NULL);
INSERT INTO `tgs15b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (NULL, 'Cat and Naps', '123 Bellvue Way', 'Tallahassee', 'FL', 4563748835, 'catnap@gmail.com', 'catnaps.com', 800000, NULL);
INSERT INTO `tgs15b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (NULL, 'Pet Palace', '400 Hayden Rd', 'Tallahassee', 'FL', 3674685878, 'ppalace@gmail.com', 'petpalace.com', 400000, NULL);
INSERT INTO `tgs15b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (NULL, 'Furry Friends', '345 Ocala Rd', 'Tallahassee', 'FL', 8498058308, 'furry@gmail.com', 'ffriends.com', 350000, NULL);
INSERT INTO `tgs15b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (NULL, 'Pets Pets Pets', '4003 Ashley Blvd', 'Tallahassee', 'FL', 7857894359, 'ppp@gmail.com', 'ppp.com', 100000, NULL);
INSERT INTO `tgs15b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (NULL, 'Cats Cats Cats', '80 Cats St', 'Tallahassee', 'FL', 5438959408, 'ccc@gmail.com', 'ccc.com', 900000, NULL);
INSERT INTO `tgs15b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (NULL, 'Pigs Pigs Pigs', '70 Pig St', 'Tallahassee', 'FL', 5989043850, 'ppps@gmail.com', 'ppps.com', 200000, NULL);
INSERT INTO `tgs15b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (NULL, DEFAULT, DEFAULT, 'Tallahassee', 'Fl', 3674685900, DEFAULT, DEFAULT, DEFAULT, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `tgs15b`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `tgs15b`;
INSERT INTO `tgs15b`.`pet` (`pet_id`, `petstore_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 2, 'Poodle', 'm', 134, 200, 34, 'brown', '04/21/1994', 'y', 'n', DEFAULT);
INSERT INTO `tgs15b`.`pet` (`pet_id`, `petstore_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 5, 'Pug', 'f', 654, 30, 79, 'brown', '05/21/1987', 'y', 'y', DEFAULT);
INSERT INTO `tgs15b`.`pet` (`pet_id`, `petstore_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 1, 'Sheepdog', 'f', 599, 399, 89, 'black', NULL, 'n', 'y', DEFAULT);
INSERT INTO `tgs15b`.`pet` (`pet_id`, `petstore_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 4, 'German Shepard', 'f', 599, 899, 39, 'white', NULL, 'n', 'y', DEFAULT);
INSERT INTO `tgs15b`.`pet` (`pet_id`, `petstore_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 2, 'Bulldog', 'm', 657, 300, 40, 'black', NULL, 'y', 'y', DEFAULT);
INSERT INTO `tgs15b`.`pet` (`pet_id`, `petstore_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 2, 'Labordudala', 'm', 100, 729, 40, 'white ', NULL, DEFAULT, DEFAULT, DEFAULT);
INSERT INTO `tgs15b`.`pet` (`pet_id`, `petstore_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 3, 'Beagle', 'f', 500, 383, 23, 'black', NULL, DEFAULT, DEFAULT, DEFAULT);
INSERT INTO `tgs15b`.`pet` (`pet_id`, `petstore_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 4, 'Lab', 'f', 100, 564, 13, 'brown', NULL, DEFAULT, DEFAULT, DEFAULT);
INSERT INTO `tgs15b`.`pet` (`pet_id`, `petstore_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 5, 'Pitbull', 'f', 200, 670, 56, 'beige', NULL, DEFAULT, DEFAULT, DEFAULT);
INSERT INTO `tgs15b`.`pet` (`pet_id`, `petstore_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 1, 'Mut', 'm', 400, 700, 45, 'white', NULL, DEFAULT, DEFAULT, DEFAULT);

COMMIT;

