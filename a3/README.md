> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced WebApplications Development

## Tucker Southerland

### Assignment #3 Requirements:

- Make ERD on MYSQL workbench
- Forward Enginneer with 10 records in each table
- Have a link to the a3.mwb & a3.sql file in bitbucket


#### README.md file should include the following items:

* Screenshot of ERD Diagram
* Links to Files on bitbucket
 



[Links to MWB file](database/a3.mwb)


[Links to SQL file](database/a3.sql)






#### Assignment Screenshots:


*Screenshot of querybook:*

![ERD diagram](img/a3.png)
