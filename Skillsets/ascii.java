import java.util.*;

public class ascii
{
    public static void main (String [] args)
    {
        //Declare
        int num = 0;
        boolean isValidNum = false;
        Scanner keys = new Scanner(System.in);
        //Characters A-Z
        System.out.println("Printing characters A-Z as ASCII values:");
        for(char character = 'A'; character <= 'z';character++)
            {
                System.out.printf("Character %c has ascii value %d\n", character, ((int)character));
            }
        //Numbers
        System.out.println("\nPrinting ASCII values 48-122 as characters:");
        for(num=48; num<=122; num++)
            {
                System.out.printf("ASCII value %d has character value %c\n", num, ((char)num));
            }
        //Print
        System.out.println("\nAllowing user ASCII value input:");
        
        while (isValidNum == false)
            {
                System.out.print("Please enter ASCII value (32-127): " );
                if (keys.hasNextInt())
                    {
                        num = keys.nextInt();
                        isValidNum=true;
                    }
                else
                    {
                        System.out.println("Invalid integer--ASCII value must be a number.\n");
                    }
                keys.nextLine();
        
                
        if (isValidNum == true && num < 32 || num > 127)
            {
            System.out.println("ASCII value must be >=32 and <=127.\n");
            isValidNum = false;   
            }
            
        if (isValidNum == true)
            {
                System.out.printf("\nASCII value %d has character value %c\n", num, ((char)num));
                }
        }
    }
}
        