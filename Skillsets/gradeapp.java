import java.util.Scanner;

public class gradeapp
{
   public static void main(String[] args)
   {
      Scanner sc = new Scanner(System.in);
      
      int numberOfGrades = 0;
      float grade = 0;
      float sum = 0;


      System.out.println("Enter grades in range of 0 - 100. (Type -1 when you are done!");

      while(grade>=0 &&  grade<=100)
      {
         System.out.print("Enter grade: ");
         grade = sc.nextFloat();
         if(grade>=0 && grade<=100){
           
           sum=sum + grade;
           numberOfGrades= numberOfGrades + 1;

         }
         else if(grade!=-1){
         System.out.print("Invalid entry, Not counted");
         grade=0;


         }

      }
   System.out.printf("\nCount: %d \nTotal: %.2f \nAverage: %.2f" , numberOfGrades, sum , sum/numberOfGrades);

      


   }

}