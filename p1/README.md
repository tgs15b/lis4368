> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced WebApplications Development

## Tucker Southerland

### Project 1 Requirements:

- Clone student files
- Clone locations for Index files and edit them
- Add controls for the customer table
- Street, city, state, zip, phone, email, balance, total sales, and notes
- Validate all informtion provided by the customer
- No more than 30 more characters beside state
- test for decimals and special characters for controls
- finish the P1 questions

![Homepage](img/home.png)

![Validated](img/val.png)

![Not Validated](img/notval.png)


 




