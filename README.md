> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368 - Advanced WebApplications Development

## Tucker Southerland

### Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install BitBucket 
    - Install Tomcat 

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Screenshot of Query
	- Records in Query
	- Query you need to put in data 

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Screenshot of ERD in MYSQL Workbench
	- Links to file a3.mwb
	- Links to file a3.sql

4. [P1 README.md](p1/README.md "My P1 README.md file")
	- Project file description
	- Project requirements
	- Valadation requirements

5. [A4 README.md](a4/README.md "My A4 README.md file")
	- A4 form validation - redirect to the Thank You page 
	- Putting the validators in our edited student files
	- Thank you page showing the validation was completed

5. [A5 README.md](a5/README.md)
	- Inserting data into local database
	- Preventing XSS and SQL Injection
	- Basic server side validation

6. [P2 README.md](p2/README.md)
	- Inserting data into local database
	- Preventing XSS and SQL Injection
	- Basic server side validation