> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced WebApplications Development

## Tucker Southerland

### Assignment #4 Requirements:

- Clone student files
- Edit student files to validate the information
- Direct validated form to the Thank You page



#### README.md file should include the following items:

* Screenshot of Form Input
* Screenshot of Thank You page 
 










#### Assignment Screenshots:


*Screenshot of querybook:*

![Form Input for the Database](img/form.png)

![Thank You Form Validation](img/thank.png)
